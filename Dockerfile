FROM alpine:3.14

RUN apk add --no-cache maven
RUN apk add --no-cache nodejs
RUN apk add --no-cache npm

WORKDIR /app
COPY . .

RUN mvn clean install -q -P \!it-test -DskipITs
EXPOSE 8080

WORKDIR factorial-application-boot

ENTRYPOINT ["mvn", "spring-boot:run"]