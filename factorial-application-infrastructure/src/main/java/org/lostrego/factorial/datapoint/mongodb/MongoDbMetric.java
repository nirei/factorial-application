package org.lostrego.factorial.datapoint.mongodb;

import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
// XXX: I think I recall Spring Data causing explosions on private accessors?
@SuppressWarnings("WeakerAccess")
class MongoDbMetric {

    @Id
    private String id;

    private String name;

    private int size;

    private Instant start;

    private Instant end;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return this.size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Instant getStart() {
        return this.start;
    }

    public void setStart(Instant start) {
        this.start = start;
    }

    public Instant getEnd() {
        return this.end;
    }

    public void setEnd(Instant end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "MongoDbMetric{" +
            "id='" + this.id + "'" +
            "name='" + this.name + "'" +
            ", size=" + this.size +
            ", start=" + this.start +
            ", end=" + this.end +
            "}";
    }
}
