package org.lostrego.factorial.datapoint.mongodb;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@SuppressWarnings("InterfaceNeverImplemented")
interface SpringDataDatapointRepository extends CrudRepository<MongoDbDatapoint, String>,
    CustomizedMetricRepository {

    Optional<MongoDbDatapoint> findByNameAndTime(String name, Instant time);

    List<MongoDbDatapoint> deleteByName(String name);
}
