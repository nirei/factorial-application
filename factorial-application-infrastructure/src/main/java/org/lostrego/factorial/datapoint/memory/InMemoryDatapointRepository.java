package org.lostrego.factorial.datapoint.memory;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.exception.AlreadyExistsException;
import org.lostrego.factorial.exception.ApplicationErrorCode;
import org.lostrego.factorial.exception.MetricNotFoundException;
import org.lostrego.factorial.metric.Metric;
import org.lostrego.factorial.valueobject.TimeRange;

/**
 * This is an in-memory implementation of the datapoint repository to use as a mock for testing and
 * early development stages
 */
public class InMemoryDatapointRepository implements DatapointRepository {

    private final ConcurrentMap<String, SortedSet<Datapoint>> storage = new ConcurrentHashMap<>(
        2048);

    private static Metric getMetricFromDatapoints(SortedSet<? extends Datapoint> datapoints) {
        return new Metric(
            datapoints.first().getName(),
            datapoints.size(),
            datapoints.first().getTime(),
            datapoints.last().getTime());
    }

    private static Datapoint getSearchDatapoint(String name, Instant time) {
        // We are doing some hacky stuff here with SortedSets and Comparators hence the weird
        // NaN, it's just a canary value
        return new Datapoint(name, time, Double.NaN);
    }

    @Override
    public Optional<Datapoint> find(String name, Instant time) {
        return Optional.ofNullable(this.storage.get(name))
            .map(datapoints -> datapoints.tailSet(getSearchDatapoint(name, time)))
            .filter(Predicate.not(SortedSet::isEmpty))
            .map(SortedSet::first);
    }

    @Override
    public Datapoint save(Datapoint datapoint) {
        var datapoints = this.storage.computeIfAbsent(datapoint.getName(),
            s -> new ConcurrentSkipListSet<>(Datapoint.COMPARATOR));
        if (datapoints.add(datapoint)) {
            return datapoint;
        } else {
            throw new AlreadyExistsException(ApplicationErrorCode.DATAPOINT_ALREADY_EXISTS,
                "A datapoint already exists for metric " + datapoint.getName() + " and time " +
                    datapoint.getTime());
        }
    }

    @Override
    public Optional<Metric> findMetric(String name) {
        return Optional.ofNullable(this.storage.get(name))
            .map(InMemoryDatapointRepository::getMetricFromDatapoints);
    }

    @Override
    public List<Metric> findAllMetrics() {
        return this.storage.values().stream()
            .map(InMemoryDatapointRepository::getMetricFromDatapoints)
            .collect(Collectors.toList());
    }

    @Override
    public void deleteMetric(String name) {
        if (null == this.storage.remove(name)) {
            throw new MetricNotFoundException(ApplicationErrorCode.METRIC_NOT_FOUND,
                "Unable to find metric by name " + name);
        }
    }

    @Override
    public List<Datapoint> findDatapointsInTimeRange(String name, TimeRange timeRange) {
        return Optional.ofNullable(this.storage.get(name))
            .map(datapoints -> datapoints
                .tailSet(getSearchDatapoint(name, timeRange.getStart()))
                .headSet(getSearchDatapoint(name, timeRange.getFinish())))
            .<List<Datapoint>>map(ArrayList::new)
            .orElseGet(Collections::emptyList);
    }
}
