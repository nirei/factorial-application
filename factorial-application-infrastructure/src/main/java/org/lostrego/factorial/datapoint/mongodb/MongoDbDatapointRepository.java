package org.lostrego.factorial.datapoint.mongodb;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.exception.AlreadyExistsException;
import org.lostrego.factorial.exception.ApplicationErrorCode;
import org.lostrego.factorial.exception.MetricNotFoundException;
import org.lostrego.factorial.metric.Metric;
import org.lostrego.factorial.valueobject.TimeRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;

/**
 * Implements {@link DatapointRepository} through a MongoDB instance using Spring Data
 */
@Repository
public class MongoDbDatapointRepository implements DatapointRepository {

    private final SpringDataDatapointRepository datapointRepository;

    @Autowired
    MongoDbDatapointRepository(SpringDataDatapointRepository datapointRepository) {
        this.datapointRepository = datapointRepository;
    }

    private static Metric mapToMetric(MongoDbMetric mongoDbMetric) {
        return new Metric(
            mongoDbMetric.getName(),
            mongoDbMetric.getSize(),
            mongoDbMetric.getStart(),
            mongoDbMetric.getEnd());
    }

    private static Datapoint mapToDatapoint(MongoDbDatapoint mongoDbDatapoint) {
        return new Datapoint(
            mongoDbDatapoint.getName(),
            mongoDbDatapoint.getTime(),
            mongoDbDatapoint.getValue());
    }

    private static MongoDbDatapoint mapToMongo(Datapoint datapoint) {
        return new MongoDbDatapoint(
            null,
            datapoint.getName(),
            datapoint.getTime(),
            datapoint.getValue());
    }

    @Override
    public Optional<Datapoint> find(String name, Instant time) {
        return this.datapointRepository.findByNameAndTime(name, time).map(
            MongoDbDatapointRepository::mapToDatapoint);
    }

    @Override
    public Datapoint save(Datapoint datapoint) {
        var entity = mapToMongo(datapoint);
        try {
            var result = this.datapointRepository.save(entity);
            return mapToDatapoint(result);
        } catch (DuplicateKeyException e) {
            throw new AlreadyExistsException(ApplicationErrorCode.DATAPOINT_ALREADY_EXISTS,
                "A datapoint already exists for metric " + datapoint.getName() + " and time " +
                    datapoint.getTime(), e);
        }
    }

    @Override
    public void deleteMetric(String name) {
        if (this.datapointRepository.deleteByName(name).isEmpty()) {
            throw new MetricNotFoundException(ApplicationErrorCode.METRIC_NOT_FOUND,
                "Unable to find metric by name " + name);
        }
    }

    @Override
    public List<Datapoint> findDatapointsInTimeRange(String name, TimeRange timeRange) {
        return this.datapointRepository.findDatapointsInTimeRange(
                name,
                timeRange.getStart(),
                timeRange.getFinish())
            .stream()
            .map(MongoDbDatapointRepository::mapToDatapoint)
            .collect(Collectors.toList());
    }

    @Override
    public Optional<Metric> findMetric(String name) {
        return this.datapointRepository.findMetricByName(name)
            .map(MongoDbDatapointRepository::mapToMetric);
    }

    @Override
    public List<Metric> findAllMetrics() {
        return this.datapointRepository.findAllMetrics().stream()
            .map(MongoDbDatapointRepository::mapToMetric)
            .collect(Collectors.toList());
    }
}

