package org.lostrego.factorial.datapoint.mongodb;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

interface CustomizedMetricRepository {

    Optional<MongoDbMetric> findMetricByName(String name);

    List<MongoDbMetric> findAllMetrics();

    List<MongoDbDatapoint> findDatapointsInTimeRange(String name, Instant start, Instant finish);
}
