package org.lostrego.factorial.datapoint.mongodb;

import java.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("datapoints")
// XXX: I think I recall Spring Data causing explosions on private accessors?
@SuppressWarnings("WeakerAccess")
class MongoDbDatapoint {

    @Id
    private String id;
    private String name;
    private Instant time;
    private double value;

    MongoDbDatapoint() {
    }

    MongoDbDatapoint(String id, String name, Instant time, double value) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.value = value;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getTime() {
        return this.time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public double getValue() {
        return this.value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "MongoDbDatapoint{" +
            "id='" + this.id + "'" +
            ", name='" + this.name + "'" +
            ", time=" + this.time +
            ", value=" + this.value +
            "}";
    }
}
