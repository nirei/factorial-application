package org.lostrego.factorial.datapoint.mongodb;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
class CustomizedMetricRepositoryImpl implements CustomizedMetricRepository {

    private static final GroupOperation GROUP_BY_METRIC = Aggregation.group("name")
        .first("name").as("name")
        .count().as("size")
        .min("$time").as("start")
        .max("$time").as("end");

    private final MongoTemplate mongoTemplate;


    @Autowired
    CustomizedMetricRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Optional<MongoDbMetric> findMetricByName(String name) {
        var aggregation = Aggregation.newAggregation(
            Aggregation.match(Criteria.where("name").is(name)), GROUP_BY_METRIC);

        return this.mongoTemplate.aggregate(
                aggregation,
                MongoDbDatapoint.class,
                MongoDbMetric.class)
            .getMappedResults().stream().findAny();
    }

    @Override
    public List<MongoDbMetric> findAllMetrics() {
        var aggregation = Aggregation.newAggregation(GROUP_BY_METRIC);

        return this.mongoTemplate.aggregate(
                aggregation,
                MongoDbDatapoint.class,
                MongoDbMetric.class)
            .getMappedResults();
    }

    @Override
    public List<MongoDbDatapoint> findDatapointsInTimeRange(String name, Instant start,
        Instant finish) {
        return this.mongoTemplate.find(
            Query.query(
                Criteria.where("name").is(name).andOperator(
                    Criteria.where("time").gte(start),
                    Criteria.where("time").lt(finish))
            ).with(Sort.by("time")),
            MongoDbDatapoint.class);
    }
}
