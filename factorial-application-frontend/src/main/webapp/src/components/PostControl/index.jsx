import Input from "../Input";
import DateInput from "../DateInput";
import Button from "../Button";

import { FontAwesomeIcon as Icon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import usePostDatapoint from "../../hooks/datapoint/usePostDatapoint";
import { useState } from "react";
import Datapoint from "../../model/Datapoint";
import { Status } from "../../hooks/common/useAsync";
import Spinner from "../Spinner";

function PostControl({ onPost }) {
  const [name, setName] = useState("");
  const [value, setValue] = useState("");
  const [time, setTime] = useState("");

  const { execute, status, error } = usePostDatapoint(
    name,
    new Datapoint(time, value)
  );

  return (
    <div className="container hidden md:block mx-auto pt-4 px-4">
      <div className="mb-2 font-thin">Add a new datapoint</div>
      <div className="flex space-x-4">
        <Input type="text" placeholder="Name" onChange={setName} value={name} />
        <Input
          type="text"
          placeholder="Value"
          onChange={setValue}
          value={value}
        />
        <DateInput type="text" onChange={setTime} value={time} />
        {status === Status.PENDING ? (
          <Spinner />
        ) : (
          <Button
            disabled={!name || !value || isNaN(value) || !time}
            onClick={() => execute().then(() => onPost())}
          >
            <Icon icon={faEnvelope} /> Send
          </Button>
        )}
        {status === Status.ERROR && (
          <div className="text-red-500">{JSON.stringify(error)}</div>
        )}
      </div>
    </div>
  );
}

export default PostControl;
