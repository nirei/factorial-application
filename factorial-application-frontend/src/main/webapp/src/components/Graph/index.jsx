import "chartjs-adapter-luxon";
import { useMemo } from "react";
import { Line } from "react-chartjs-2";
import { Status } from "../../hooks/common/useAsync";
import useListDatapoints from "../../hooks/datapoint/useListDatapoints";
import Spinner from "../Spinner";

function Graph({ metric, from, to }) {
  const name = metric.name;
  const { status, value, error } = useListDatapoints(name, from, to);

  const data = useMemo(
    () =>
      value && {
        datasets: [
          {
            type: "line",
            label: metric.name,
            data: value.map((datapoint) => ({
              x: datapoint.time.valueOf(),
              y: datapoint.value,
            })),
            backgroundColor: "rgb(105, 94, 232, 0.75)",
            borderColor: "rgba(105, 94, 232, 0.75)",
            borderWidth: 2,
            radius: 0,
          },
          {
            type: "bar",
            label: "Hourly average",
            data: Object.entries(
              bucket(value, (datapoint) =>
                // Since JS objects need to have key strings, we provide a canonical
                // representation (ISO format) for the date as a string for the bucket
                // names so we can parse it later on
                truncateMinutes(datapoint.time).toISOString()
              )
            ).map(([minute, datapoints]) => ({
              x: Date.parse(minute).valueOf(),
              y: average(datapoints.map((datapoint) => datapoint.value)),
            })),
            backgroundColor: "rgb(105, 94, 232, 0.3)",
            borderColor: "rgba(105, 94, 232, 0)",
            borderWidth: 2,
            radius: 0,
          },
        ],
      },
    [metric.name, value]
  );

  if (status === Status.ERROR) {
    return (
      <p className="text-red-700">{JSON.stringify(error.body.description)}</p>
    );
  }

  if (status === Status.IDLE || status === Status.PENDING) {
    return <Spinner />;
  }

  let options = {
    scales: {
      x: {
        type: "time",
        time: {
          displayFormats: {
            hour: "HH:MM",
          },
        },
        min: from,
        max: to,
      },
      y: {
        min: 0,
      },
    },
    parsing: false,
    plugins: {
      decimation: {
        enabled: true,
        algorithm: "lttb",
      },
    },
    elements: {
      line: {
        tension: 0, // disables bezier curves
      },
    },
    showLines: false, // disable for all datasets
    responsiveAnimationDuration: 0, // animation duration after a resize
  };

  // When there's a lot of data, disable animations to improve performance
  if (value.length > ANIMATION_DISABLE_THRESHOLD) {
    options["animation"] = { duration: 0 };
  }

  return <Line data={data} options={options} />;
}

const ANIMATION_DISABLE_THRESHOLD = 200;

/**
 * Bucket array values in bins defined by a keying function
 * @param {any[]} array the array to bucket
 * @param {function(any): any} keyFun a function that, given an item from the array, returns its corresponding bucket
 * @returns {Object.<any, any[]} an object whose keys are the generated buckets, each containing an array of the values for that bucket
 */
function bucket(array, keyFun) {
  const buckets = {};
  array.forEach((item) => {
    const key = keyFun(item);
    if (!buckets[key]) {
      buckets[key] = [];
    }
    buckets[key].push(item);
  });
  return buckets;
}

/**
 * Given a date, return the same date truncated to minute precision
 * @param {Date} date
 * @returns {Date} the input date truncated to minute precision
 */
function truncateMinutes(date) {
  return new Date(
    date.getFullYear(),
    date.getMonth(),
    date.getDate(),
    date.getHours()
  );
}

/**
 * @param {number[]} array an array of numbers
 * @returns number the average of the values in the array
 */
function average(array) {
  return array.reduce((a, b) => a + b) / array.length;
}

export default Graph;
