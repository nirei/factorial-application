function Button({ disabled, ...props }) {
  return (
    <button
      className={`${
        disabled ? "bg-gray-500" : "bg-blue-500 hover:bg-blue-700"
      } text-white font-bold px-4 rounded whitespace-nowrap`}
      disabled={disabled}
      {...props}
    />
  );
}

export default Button;
