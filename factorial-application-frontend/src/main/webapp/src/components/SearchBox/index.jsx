import { FontAwesomeIcon as Icon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import DateInput from "../DateInput";
import { useState } from "react";
import Button from "../Button";

function SearchBox({ earliest, latest, timeRange, onTimeRange }) {
  const [from, setFrom] = useState(timeRange.from);
  const [to, setTo] = useState(timeRange.to);

  return (
    <div className="flex space-x-4 lg:w-full xl:w-2/3">
      <p className="sm:hidden whitespace-nowrap leading-10">Time range</p>
      <DateInput
        placeholder="Starting time"
        onChange={setFrom}
        value={from}
        min={earliest}
        max={latest}
      />
      <DateInput
        placeholder="Ending time"
        onChange={setTo}
        value={to}
        min={earliest}
        max={latest}
      />
      <Button onClick={() => onTimeRange({ from, to })}>
        <Icon icon={faSearch} />
      </Button>
    </div>
  );
}

export default SearchBox;
