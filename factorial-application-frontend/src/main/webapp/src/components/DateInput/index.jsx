import Input from "../Input";

function toInput(value) {
  // It seems that <input type="datetime-local"> accepts only something an ISO 8601
  // local date-time string (no timezone) as value. ECMA does not seem to provide
  // an obvious way to get such string from a Date object so we're forced to do it by hand
  const year = value.getFullYear();
  const month = (value.getMonth() + 1).toString().padStart(2, "0");
  const day = value.getDate().toString().padStart(2, "0");
  const hour = value.getHours().toString().padStart(2, "0");
  const minute = value.getMinutes().toString().padStart(2, "0");
  return `${year}-${month}-${day}T${hour}:${minute}`;
}

function DateInput({ placeholder, icon, onChange, value, min, max }) {
  const dateValue = value && toInput(value);
  const dateMin = min && toInput(min);
  const dateMax = max && toInput(max);

  return (
    <Input
      type="datetime-local"
      value={dateValue}
      min={dateMin}
      max={dateMax}
      onChange={(value) => onChange && onChange(new Date(value))}
      placeholder={placeholder}
      icon={icon}
    />
  );
}

export default DateInput;
