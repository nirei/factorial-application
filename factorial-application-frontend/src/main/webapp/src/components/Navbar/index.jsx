function Navbar() {
    return (
      <div className="flex items-center justify-center bg-blue-600 bg-opacity-75 shadow-lg">
        <div className="container mx-auto text-3xl text-white font-light leading-loose px-4">
          <h1>Metrics Visualizer</h1>
        </div>
      </div>
    );
}

export default Navbar;