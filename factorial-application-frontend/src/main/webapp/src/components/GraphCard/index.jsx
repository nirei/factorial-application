import Graph from "../Graph";

function GraphCard({ hidden, metric, from, to }) {
  return (
    <div
      className={`x-space-2 my-1 bg-cover bg-center group rounded-lg overflow-hidden shadow-lg ${
        hidden ? "hidden" : ""
      }`}
    >
      <div className="relative p-4 flex flex-col h-64">
        <h3 className="text-md uppercase font-thin mb-2">{metric.name}</h3>
        <div className="my-auto">
          <Graph metric={metric} from={from} to={to} />
        </div>
      </div>
    </div>
  );
}

export default GraphCard;
