import MetricCard from "../MetricCard";

function Sidebar({
  metrics,
  hiddenMetrics,
  pushHiddenMetric,
  removeHiddenMetric,
}) {
  return (
    <aside className="hidden md:flex flex-col w-72 md:w-96 overflow-y-auto pr-4">
      {metrics.map((metric) => (
        <MetricCard
          key={metric.name}
          metric={metric}
          hidden={hiddenMetrics.includes(metric.name)}
          handleShow={() => removeHiddenMetric(metric.name)}
          handleHide={() => pushHiddenMetric(metric.name)}
        />
      ))}
    </aside>
  );
}

export default Sidebar;
