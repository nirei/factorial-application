import { FontAwesomeIcon as Icon } from "@fortawesome/react-fontawesome";

function Input({ icon, onChange, ...props }) {
  return (
    <div className="relative flex w-full flex-wrap items-stretch">
      {icon && (
        <span className="z-10 h-full leading-snug font-normal absolute text-center text-gray-400 absolute bg-transparent rounded text-base items-center justify-center w-8 pl-3 py-3">
          <Icon icon={icon} />
        </span>
      )}
      <input
        className={`px-3 py-3 placeholder-gray-400 text-gray-600 relative bg-white bg-white rounded text-sm border border-gray-400 outline-none focus:outline-none focus:ring w-full ${
          icon ? "pl-10" : ""
        }`}
        onChange={(event) => onChange && onChange(event.target.value)}
        {...props}
      />
    </div>
  );
}

export default Input;
