import { useEffect, useState } from "react";
import useStateArray from "../../hooks/common/useStateArray";
import useMetricsInformation from "../../hooks/metric/useMetricsInformation";
import { Status } from "../../hooks/common/useAsync";

import Sidebar from "../Sidebar";
import GraphCard from "../GraphCard";
import SearchBox from "../SearchBox";
import Spinner from "../Spinner";

function MainContent({ listMetricsState: { status, value: metrics, error } }) {
  const information = useMetricsInformation(metrics);
  const [timeRange, setTimeRange] = useState(null);
  const [hiddenMetrics, , pushHiddenMetric, removeHiddenMetric] =
    useStateArray();

  // Set initial time range when metrics information arrives
  useEffect(
    () =>
      information.earliest &&
      setTimeRange((previous) =>
        previous === null
          ? {
              from: new Date(+information.earliest),
              to: new Date(+information.earliest + 1000 * 60 * 60 * 8),
            }
          : previous
      ),
    [information]
  );

  if (
    timeRange === null ||
    status === Status.IDLE ||
    status === Status.PENDING
  ) {
    return (
      <div className="container m-auto pt-4 px-4 h-screen">
        <div className="flex items-center justify-center h-full">
          <Spinner />
        </div>
      </div>
    );
  }

  if (status === Status.ERROR) {
    return "Error: " + JSON.stringify(error);
  }

  return (
    <div className="container m-auto flex pt-4 px-4 h-4/5 overflow-hidden">
      <Sidebar
        metrics={information.sorted}
        hiddenMetrics={hiddenMetrics}
        pushHiddenMetric={pushHiddenMetric}
        removeHiddenMetric={removeHiddenMetric}
      />
      <main className="w-full overflow-y-auto">
        <div className="hidden md:flex justify-items-end flex-row-reverse px-4">
          <SearchBox
            earliest={information.earliest}
            latest={information.latest}
            timeRange={timeRange}
            onTimeRange={setTimeRange}
          />
        </div>
        {information.sorted.length === hiddenMetrics.length && (
          <div className="text-center my-12">Please, select some data to visualize</div>
        )}
        <div className="grid gap-4 grid-cols-1 lg:grid-cols-2 xl:grid-cols-3 px-4">
          {information.sorted.map((metric) => (
            <GraphCard
              key={metric.name}
              hidden={hiddenMetrics.includes(metric.name)}
              metric={metric}
              from={timeRange.from}
              to={timeRange.to}
            />
          ))}
        </div>
      </main>
    </div>
  );
}

export default MainContent;
