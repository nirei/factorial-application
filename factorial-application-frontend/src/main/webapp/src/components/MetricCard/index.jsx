import { FontAwesomeIcon as Icon } from "@fortawesome/react-fontawesome";
import {
  faEye,
  faEyeSlash,
  faHourglassStart,
  faHourglassEnd,
} from "@fortawesome/free-solid-svg-icons";

const LOCALE_STRING_OPTIONS = {
  year: "numeric",
  month: "long",
  day: "numeric",
  hour: "numeric",
  minute: "numeric",
  second: "numeric",
};

function ColoredCard({ hidden, children }) {
  return (
    <div
      className={`${
        hidden ? "bg-gray-500" : "bg-blue-600"
      } bg-opacity-75 mb-2 py-3 px-4 rounded-lg shadow-lg transition duration-300 ease-in-out w-full`}
    >
      {children}
    </div>
  );
}

function DateField({ hidden, children }) {
  const className = `hidden sm:hidden md:block text-sm mt-2 ${
    hidden ? "text-gray-100" : "text-blue-100"
  }`;
  return <p className={className}>{children}</p>;
}

function MetricCard({
  metric: { name, size, start, end },
  hidden,
  handleShow,
  handleHide,
}) {
  const startDate = start.toLocaleString("default", LOCALE_STRING_OPTIONS);
  const endDate = end.toLocaleString("default", LOCALE_STRING_OPTIONS);

  return (
    <ColoredCard hidden={hidden}>
      <div className="text-white truncate flex space-x-2 items-center font-bold">
        <button
          onClick={hidden ? handleShow : handleHide}
          className={`bg-white rounded-md w-8 px-1 py-1 flex items-center transition duration-300 ease-in-out ${
            hidden
              ? "text-gray-300 hover:text-gray-600"
              : "text-blue-300 hover:text-blue-600"
          }`}
        >
          <Icon className="mx-auto" icon={hidden ? faEyeSlash : faEye} />
        </button>
        <p>{name}</p>
      </div>
      <h3 className="hidden md:block text-white text-sm mt-2 font-bold">
        {size} datapoints
      </h3>
      <DateField hidden={hidden}>
        <Icon className="text-xs mr-1" icon={faHourglassStart} />
        {startDate}
      </DateField>
      <DateField hidden={hidden}>
        <Icon className="text-xs mr-1" icon={faHourglassEnd} />
        {endDate}
      </DateField>
    </ColoredCard>
  );
}

export default MetricCard;
