import DatapointApi from "../../api/DatapointApi";
import useAsync from "../common/useAsync";

const api = new DatapointApi();

function postDatapoint(metric, datapoint) {
  return api.postMetricNamePoint(metric, { datapoint });
}

function usePostDatapoint(metric, datapoint) {
  return useAsync(postDatapoint, [metric, datapoint], false);
}

export default usePostDatapoint;
