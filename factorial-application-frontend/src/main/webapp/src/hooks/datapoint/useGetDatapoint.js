import DatapointApi from "../../api/DatapointApi";
import useAsync from "../common/useAsync";

const api = new DatapointApi();

function getDatapoint(name, time) {
  return api.getMetricNamePointTime();
}

function useGetDatapoint(name, time) {
  return useAsync(getDatapoint, [name, time]);
}

export default useGetDatapoint;
