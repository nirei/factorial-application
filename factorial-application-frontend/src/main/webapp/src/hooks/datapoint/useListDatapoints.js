import DatapointApi from "../../api/DatapointApi";
import useAsync from "../common/useAsync";

const api = new DatapointApi();

function listDatapoints(name, from, to) {
  return api.listMetricNamePoints(name, from, to);
}

function useListDatapoints(name, from, to) {
  return useAsync(listDatapoints, [name, from, to]);
}

export default useListDatapoints;
