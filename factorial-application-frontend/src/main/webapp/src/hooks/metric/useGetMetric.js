import MetricApi from "../../api/MetricApi";
import useAsync from "../common/useAsync";

const api = new MetricApi();

function getMetric(name) {
  return api.getMetricName(name);
}

function useGetMetric(name) {
  return useAsync(getMetric, [name]);
}

export default useGetMetric;
