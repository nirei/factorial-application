import MetricApi from "../../api/MetricApi";
import useAsync from "../common/useAsync";

const api = new MetricApi();

function deleteMetric(name) {
  return api.deleteMetricName(name);
}

function useDeleteMetric(name) {
  return useAsync(deleteMetric, [name], false);
}

export default useDeleteMetric;
