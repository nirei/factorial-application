import { useMemo } from "react";

function useMetricsInformation(metrics) {
  return useMemo(
    () => ({
      // metrics sorted in alphabetical order
      sorted:
        metrics &&
        [...metrics].sort((first, second) =>
          first.name.localeCompare(second.name)
        ),
      // earliest date there's any data for
      earliest:
        metrics &&
        new Date(metrics
          .map((metric) => metric.start.getTime())
          .sort()
          .at(0)),
      // latest date there's any date for
      latest:
        metrics &&
        new Date(metrics
          .map((metric) => metric.end.getTime())
          .sort()
          .at(-1)),
    }),
    [metrics]
  );
}

export default useMetricsInformation;
