import MetricApi from "../../api/MetricApi";
import useAsync from "../common/useAsync";

const api = new MetricApi();

const argument = [];

function listMetrics() {
  return api.listMetrics();
}

function useListMetrics() {
  return useAsync(listMetrics, argument);
}

export default useListMetrics;
