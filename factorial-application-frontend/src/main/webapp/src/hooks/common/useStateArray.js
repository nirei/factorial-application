import { useState } from "react";

/**
 * Provides a convenient wrapper around React's useState() to handle array states.
 * In addition to the usual setArray, it returns two other functions that allow to
 * push and remove elements from the state array.
 * @param {Array} initialValue initial array value that will be passed to useState,
 * empty by default
 * @returns an Array containing the state value, the setter function to overwrite
 * the state, a push function and a remove function
 */
function useStateArray(initialValue = []) {
  const [array, setArray] = useState(initialValue);

  return [
    array,
    setArray,
    (element) => setArray([...array, element]),
    (element) => setArray(array.filter((content) => content !== element)),
  ];
}

export default useStateArray;
