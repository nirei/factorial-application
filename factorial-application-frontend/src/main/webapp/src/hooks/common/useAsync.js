import { useState, useEffect, useCallback } from "react";

const Status = {
  IDLE: "IDLE",
  PENDING: "PENDING",
  SUCCESS: "SUCCESS",
  ERROR: "ERROR",
};

export { Status };

function useAsync(asyncFunction, parameters, immediate = true) {
  const [status, setStatus] = useState(Status.IDLE);
  const [value, setValue] = useState(null);
  const [error, setError] = useState(null);

  // The execute function wraps asyncFunction and
  // handles setting state for pending, value, and error.
  // useCallback ensures the below useEffect is not called
  // on every render, but only if asyncFunction changes.
  const execute = useCallback(() => {
    setStatus(Status.PENDING);
    setValue(null);
    setError(null);

    const promise = asyncFunction(...parameters);
    promise
      .then((response) => {
        setValue(response);
        setStatus(Status.SUCCESS);
      })
      .catch((failure) => {
        setError(failure);
        setStatus(Status.ERROR);
      });

    return promise;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [asyncFunction, ...parameters]);

  // Call execute if we want to fire it right away.
  // Otherwise execute can be called later, such as
  // in an onClick handler.
  useEffect(() => {
    if (immediate) {
      execute();
    }
  }, [execute, immediate]);

  return { execute, status, value, error };
}

export default useAsync;
