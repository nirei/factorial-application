import "./index.css";
import useListMetrics from "./hooks/metric/useListMetrics";

import Navbar from "./components/Navbar";
import PostControl from "./components/PostControl";
import MainContent from "./components/MainContent";

function App() {
  const listMetricsState = useListMetrics();

  return (
    <div className="flex flex-col w-full max-h-screen">
      <Navbar />
      <PostControl onPost={listMetricsState.execute} />
      <MainContent listMetricsState={listMetricsState} />
    </div>
  );
}

export default App;
