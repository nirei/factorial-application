const colors = require("tailwindcss/colors");

module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    colors: {
      blue: {
        900: "#373179",
        700: "#534bb8",
        600: "#695ee8",
        500: "#877ef4",
        300: "#a19ce2",
        100: "#cbc8ef",
      },
      white: colors.white,
      gray: colors.trueGray,
    },
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      "2xl": "1536px",
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
