package org.lostrego.factorial.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@SuppressWarnings("MethodMayBeStatic")
public class CorsConfiguration {

    @Bean
    @Profile("dev")
    public WebMvcConfigurer corsConfigurer() {
        return new CorsWebMvcConfigurer("http://localhost:3000", "http://192.168.1.21:3000");
    }

    private static final class CorsWebMvcConfigurer implements WebMvcConfigurer {

        private final String[] allowedOrigins;

        private CorsWebMvcConfigurer(String... allowedOrigins) {
            this.allowedOrigins = allowedOrigins;
        }

        @Override
        public void addCorsMappings(CorsRegistry registry) {
            registry.addMapping("/**").allowedOrigins(this.allowedOrigins);
        }
    }
}
