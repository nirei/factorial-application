package org.lostrego.factorial.config;

import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.datapoint.memory.InMemoryDatapointRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@SuppressWarnings("MethodMayBeStatic")
public class StandalonePersistenceConfiguration {

    @Bean
    @Profile("local")
    DatapointRepository datapointRepository() {
        return new InMemoryDatapointRepository();
    }
}
