db.createUser(
  {
    user: "metricssvc",
    pwd: "test",
    roles: [
      {
        role: "readWrite",
        db: "metrics"
      }
    ]
  }
);

db.datapoints.createIndex(
  {
    "name": 1,
    "time": 1
  },
  { unique: true }
);