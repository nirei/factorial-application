package org.lostrego.factorial.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@TestConfiguration
@EnableAutoConfiguration
@EnableMongoRepositories(basePackages = "org.lostrego.factorial")
public class IntegrationTestConfiguration {

}
