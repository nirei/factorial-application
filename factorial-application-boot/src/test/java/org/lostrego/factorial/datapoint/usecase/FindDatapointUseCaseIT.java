package org.lostrego.factorial.datapoint.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.lostrego.factorial.config.IntegrationTestConfiguration;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.datapoint.mongodb.MongoDbDatapointRepository;
import org.lostrego.factorial.exception.DatapointNotFoundException;
import org.lostrego.factorial.exception.MetricNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ActiveProfiles("test")
@SpringBootTest(classes = {
    IntegrationTestConfiguration.class,
    MongoDbDatapointRepository.class,
    FindDatapointUseCaseImpl.class
})
@ExtendWith(SpringExtension.class)
class FindDatapointUseCaseIT {

    @BeforeEach
    void resetDatabase(@Autowired MongoTemplate mongoTemplate) {
        mongoTemplate.remove(new Query(), "datapoints");
    }

    @BeforeEach
    void cleanUp(@Autowired DatapointRepository datapointRepository) {
        try {
            datapointRepository.deleteMetric("testing-metric");
        } catch (MetricNotFoundException e) {
            // There was nothing to clean up, ignore
        }
    }

    @Test
    void testFindDatapoint_happyPath(
        @Autowired DatapointRepository datapointRepository,
        @Autowired FindDatapointUseCase findDatapointUseCase) {
        var datapoint = new Datapoint("testing-metric", Instant.now(), 13.37);
        datapointRepository.save(datapoint);

        var result = findDatapointUseCase.findDatapoint(datapoint.getName(), datapoint.getTime());

        assertEquals(datapoint, result, "Persisted entity does not match");
    }

    @Test
    void testFindDatapoint_metricNotFound(
        @Autowired DatapointRepository datapointRepository,
        @Autowired FindDatapointUseCase findDatapointUseCase) {
        var now = Instant.now();
        assertThrows(DatapointNotFoundException.class,
            () -> findDatapointUseCase.findDatapoint("testing-metric", now));
    }

    @Test
    void testFind_datapointNotFound(
        @Autowired DatapointRepository datapointRepository,
        @Autowired FindDatapointUseCase findDatapointUseCase) {
        var now = Instant.now();
        var datapoint = new Datapoint("testing-metric", Instant.now().minus(
            1L,
            ChronoUnit.SECONDS),
            13.37);
        datapointRepository.save(datapoint);

        assertThrows(DatapointNotFoundException.class,
            () -> findDatapointUseCase.findDatapoint("testing-metric", now));

    }
}
