package org.lostrego.factorial.datapoint.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.lostrego.factorial.config.IntegrationTestConfiguration;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.datapoint.mongodb.MongoDbDatapointRepository;
import org.lostrego.factorial.exception.MetricNotFoundException;
import org.lostrego.factorial.valueobject.TimeRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ActiveProfiles("test")
@SpringBootTest(classes = {
    IntegrationTestConfiguration.class,
    MongoDbDatapointRepository.class,
    ListDatapointsUseCaseImpl.class
})
@ExtendWith(SpringExtension.class)
class ListDatapointsUseCaseIT {

    @BeforeEach
    void resetDatabase(@Autowired MongoTemplate mongoTemplate) {
        mongoTemplate.remove(new Query(), "datapoints");
    }

    @BeforeEach
    void cleanUp(@Autowired DatapointRepository datapointRepository) {
        try {
            datapointRepository.deleteMetric("testing-metric");
        } catch (MetricNotFoundException e) {
            // There was nothing to clean up, ignore
        }
    }

    @Test
    void testListDatapoints_happyPath(
        @Autowired DatapointRepository datapointRepository,
        @Autowired ListDatapointsUseCase listDatapointsUseCase) {
        var metricName = "testing-metric";
        var start = Instant.now().minus(5L, ChronoUnit.SECONDS);
        var end = Instant.now();

        var datapoint1 = new Datapoint(metricName, start, 42.0);
        var datapoint2 = new Datapoint(metricName, end, 43.0);
        datapointRepository.save(datapoint1);
        datapointRepository.save(datapoint2);

        // Test on different time ranges
        var result1 = listDatapointsUseCase.listDatapoints(metricName,
            new TimeRange(start, end.plus(1L, ChronoUnit.SECONDS)));
        assertEquals(List.of(datapoint1, datapoint2), result1, "Persisted entity does not match");

        var result2 = listDatapointsUseCase.listDatapoints(metricName,
            new TimeRange(start, end.minus(1L, ChronoUnit.SECONDS)));
        assertEquals(List.of(datapoint1), result2, "Persisted entity does not match");

        var result3 = listDatapointsUseCase.listDatapoints(metricName,
            new TimeRange(start.plus(1L, ChronoUnit.SECONDS), end.plus(1L, ChronoUnit.SECONDS)));
        assertEquals(List.of(datapoint2), result3, "Persisted entity does not match");

        var result4 = listDatapointsUseCase.listDatapoints(metricName,
            new TimeRange(start.plus(1L, ChronoUnit.SECONDS), end.minus(1L, ChronoUnit.SECONDS)));
        assertEquals(List.of(), result4, "Persisted entity does not match");
    }

    @Test
    void testListDatapoints_metricNotFound(
        @Autowired DatapointRepository datapointRepository,
        @Autowired ListDatapointsUseCase listDatapointsUseCase) {
        var now = Instant.now();
        var timeRange = new TimeRange(Instant.now(), Instant.now());
        assertThrows(MetricNotFoundException.class,
            () -> listDatapointsUseCase.listDatapoints("testing-metric", timeRange));
    }
}