package org.lostrego.factorial.datapoint.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.lostrego.factorial.config.IntegrationTestConfiguration;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.datapoint.mongodb.MongoDbDatapointRepository;
import org.lostrego.factorial.exception.AlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ActiveProfiles("test")
@SpringBootTest(classes = {
    IntegrationTestConfiguration.class,
    MongoDbDatapointRepository.class,
    AddDatapointUseCaseImpl.class
})
@ExtendWith(SpringExtension.class)
class AddDatapointUseCaseIT {

    @BeforeEach
    void resetDatabase(@Autowired MongoTemplate mongoTemplate) {
        mongoTemplate.remove(new Query(), "datapoints");
    }

    @Test
    void testAddDatapoint_happyPath(
        @Autowired DatapointRepository datapointRepository,
        @Autowired AddDatapointUseCase addDatapointUseCase) {
        var datapoint = new Datapoint("testing-metric", Instant.now(), 13.37);
        var result = addDatapointUseCase.addDatapoint(datapoint);

        assertEquals(datapoint, result, "Datapoints do not match");

        var persisted = datapointRepository.find(datapoint.getName(), datapoint.getTime());
        assertTrue(persisted.isPresent(), "Persisted entity not found");
        assertEquals(datapoint, persisted.get(), "Persisted entity does not match");
    }

    @Test
    void testAddDatapoint_alreadyExists(
        @Autowired DatapointRepository datapointRepository,
        @Autowired AddDatapointUseCase addDatapointUseCase) {
        var time = Instant.now();
        var existing = new Datapoint("testing-metric", time, 13.37);
        datapointRepository.save(existing);

        var datapoint = new Datapoint("testing-metric", time, 6.685);
        assertThrows(AlreadyExistsException.class,
            () -> addDatapointUseCase.addDatapoint(datapoint));
    }

}
