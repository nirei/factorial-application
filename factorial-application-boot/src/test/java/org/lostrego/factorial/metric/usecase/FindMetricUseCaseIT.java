package org.lostrego.factorial.metric.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.lostrego.factorial.config.IntegrationTestConfiguration;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.datapoint.mongodb.MongoDbDatapointRepository;
import org.lostrego.factorial.exception.MetricNotFoundException;
import org.lostrego.factorial.metric.Metric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ActiveProfiles("test")
@SpringBootTest(classes = {
    IntegrationTestConfiguration.class,
    MongoDbDatapointRepository.class,
    FindMetricUseCaseImpl.class
})
@ExtendWith(SpringExtension.class)
class FindMetricUseCaseIT {

    @BeforeEach
    void resetDatabase(@Autowired MongoTemplate mongoTemplate) {
        mongoTemplate.remove(new Query(), "datapoints");
    }

    @Test
    void testFindMetric_happyPath(
        @Autowired DatapointRepository datapointRepository,
        @Autowired FindMetricUseCase findMetricUseCase) {

        var metricName = "testing-metric";

        var firstInstant = Instant.now().minus(12L, ChronoUnit.SECONDS);
        var datapoint1 = new Datapoint(metricName, firstInstant, 13.37);
        datapointRepository.save(datapoint1);
        assertEquals(new Metric(metricName, 1, firstInstant, firstInstant),
            findMetricUseCase.findMetric(metricName),
            "Metric doesn't match");

        var secondInstant = Instant.now();
        var datapoint2 = new Datapoint(metricName, secondInstant, 734.53);
        datapointRepository.save(datapoint2);
        assertEquals(new Metric(metricName, 2, firstInstant, secondInstant),
            findMetricUseCase.findMetric(metricName),
            "Metric doesn't match");

        var thirdInstant = Instant.now().minus(24L, ChronoUnit.SECONDS);
        var datapoint3 = new Datapoint(metricName, thirdInstant, 512.0);
        datapointRepository.save(datapoint3);
        assertEquals(new Metric(metricName, 3, thirdInstant, secondInstant),
            findMetricUseCase.findMetric(metricName),
            "Metric doesn't match");
    }

    @Test
    void testFindMetric_notFound(
        @Autowired DatapointRepository datapointRepository,
        @Autowired FindMetricUseCase findMetricUseCase) {
        assertThrows(MetricNotFoundException.class,
            () -> findMetricUseCase.findMetric("test-metric"));
    }
}
