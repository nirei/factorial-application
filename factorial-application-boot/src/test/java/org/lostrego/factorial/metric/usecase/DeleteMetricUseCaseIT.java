package org.lostrego.factorial.metric.usecase;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.lostrego.factorial.config.IntegrationTestConfiguration;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.datapoint.mongodb.MongoDbDatapointRepository;
import org.lostrego.factorial.exception.MetricNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ActiveProfiles("test")
@SpringBootTest(classes = {
    IntegrationTestConfiguration.class,
    MongoDbDatapointRepository.class,
    DeleteMetricUseCaseImpl.class
})
@ExtendWith(SpringExtension.class)
class DeleteMetricUseCaseIT {

    @BeforeEach
    void resetDatabase(@Autowired MongoTemplate mongoTemplate) {
        mongoTemplate.remove(new Query(), "datapoints");
    }

    @Test
    void testDeleteMetric_happyPath(
        @Autowired DatapointRepository datapointRepository,
        @Autowired DeleteMetricUseCase deleteMetricUseCase) {
        var metricName = "testing-metric";
        var datapoint1 = new Datapoint(
            metricName,
            Instant.now().minus(12L, ChronoUnit.SECONDS),
            13.37);
        var datapoint2 = new Datapoint(metricName, Instant.now(), 734.53);
        datapointRepository.save(datapoint1);
        datapointRepository.save(datapoint2);

        deleteMetricUseCase.deleteMetric(metricName);
        assertTrue(datapointRepository.findMetric(metricName).isEmpty(), "Metric was not deleted");
    }

    @Test
    void testDeleteDatapoints_notFound(
        @Autowired DatapointRepository datapointRepository,
        @Autowired DeleteMetricUseCase deleteMetricUseCase) {
        assertThrows(MetricNotFoundException.class,
            () -> deleteMetricUseCase.deleteMetric("testing-metric"));
    }

}