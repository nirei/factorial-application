package org.lostrego.factorial.metric.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.lostrego.factorial.config.IntegrationTestConfiguration;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.datapoint.mongodb.MongoDbDatapointRepository;
import org.lostrego.factorial.metric.Metric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ActiveProfiles("test")
@SpringBootTest(classes = {
    IntegrationTestConfiguration.class,
    MongoDbDatapointRepository.class,
    ListMetricsUseCaseImpl.class
})
@ExtendWith(SpringExtension.class)
class ListMetricUseCaseIT {

    @BeforeEach
    void resetDatabase(@Autowired MongoTemplate mongoTemplate) {
        mongoTemplate.remove(new Query(), "datapoints");
    }

    @Test
    void testListMetric_happyPath(
        @Autowired DatapointRepository datapointRepository,
        @Autowired ListMetricsUseCase listMetricUseCase) {

        assertEquals(List.of(), listMetricUseCase.listMetrics(),
            "Empty repository produces metrics");

        var datapoint1 = new Datapoint("testing-metric1",
            Instant.now().minus(12L, ChronoUnit.SECONDS),
            13.37);
        var datapoint2 = new Datapoint("testing-metric2", Instant.now(), 734.53);
        datapointRepository.save(datapoint1);
        datapointRepository.save(datapoint2);

        // Order is irrelevant, compare sets, not lists
        assertEquals(Set.of(datapoint1.getName(), datapoint2.getName()),
            Set.of(listMetricUseCase.listMetrics().stream().map(Metric::getName).toArray()),
            "Listed metric names do not match");

        var datapoint3 = new Datapoint("testing-metric1",
            Instant.now().minus(24L, ChronoUnit.SECONDS),
            512.0);
        datapointRepository.save(datapoint3);

        // Adding a new datapoint on an existing metric should not change metrics list
        assertEquals(Set.of(datapoint1.getName(), datapoint2.getName()),
            Set.of(listMetricUseCase.listMetrics().stream().map(Metric::getName).toArray()),
            "Listed metric names do not match");
    }

}
