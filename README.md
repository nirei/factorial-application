## Introduction ##

Metrics Visualizer is a simple web application to watch and explore metrics, understood as series of
numerical values over time. Metrics Visualizer is open-source software, published under the MIT
License.

## General ##

This is the repository for Metrics Visualizer, where its full source code can be downloaded and
where development efforts are coordinated.

The software consists of a simple Single-Page Application in React and a REST API served from a
Spring Boot backend. The definition for the API to communicate with the backend can be found in
OpenAPI format in [the api module](factorial-application-api/src/main/resources/metrics.yml).

The software revolves around two simple concepts, namely:

* Metrics: Series of data identified by a name
* Datapoints: Timestamped values associated with a metric

## Container set up ##

You can run the application as a docker container using `docker-compose`.

```shell
docker-compose build
docker-compose up
```

At this point, the application will be up and running, being served
at [http://localhost:8080](http://localhost:8080).

## Development set up ##

### Requirements ###

To run the application from source you need to have a copy of the Java Development Kit version 11 or
greater installed on your system. You can find instructions on how to set up the
JDK [here](https://docs.oracle.com/en/java/javase/11/install/overview-jdk-installation.html).

Metrics Visualizer uses the Apache Maven build system for the compilation process. You can get
detailed instructions for its set up in
the [Installing Apache Maven](https://maven.apache.org/install.html) section of its official
documentation.

For the frontend build, Node.js and npm are used. Precise installation instructions are given at
the [How to install Node.js](https://nodejs.dev/learn/how-to-install-nodejs/) page on Node.js
documentation.

Furthermore, if you wish to be able to run integration tests you will also need to have docker
installed on your system. Download links and installation instructions can be found
in [Install Docker Engine](https://docs.docker.com/engine/install/), from its official docs.

### Build process ###

To commence the build process you may execute the following command on a command line:

```shell
$ mvn clean install
```

This will:

1. Install required dependencies
2. Generate client (JS) and controller (Java) code from the OpenAPI definition
3. Compile both backend and frontend
4. Raise a docker container with MongoDB, you can skip this and the next point by
   adding `-Dmaven.test.skip=true`
5. Run integration tests against said Docker
6. Package the application in a `.jar` file

After this is finished, simply run:

```shell
$ mvn spring-boot:run
```

And the application will start. You can access it through your browser
in [http://localhost:8080](http://localhost:8080).

### Set up with MongoDB ###

By default, the application will run with non-persistent storage in RAM. If want to set it up
against a MongoDB database, this is how you need to proceed.

In the first place, the `local` profile, responsible for the in-memory storage must be deactivated
by removing it from `factorial-application-boot/src/main/resources/application.yml`.

```yml
spring:
  profiles:
    active:
      - local # remove this line
```

Afterwards, in `factorial-application-boot/src/main/resources/application.yml` add the following
information.

```yml
spring:
  data:
    mongodb:
      host: <hostname of the mongodb instance>
      port: <port of the mongodb instance>
      database: <name of the database>
      username: <username used to connect to the database>
      password: <password used to connect to the database>
```

Another acceptable syntax is:

```yml
spring:
  data:
    mongodb:
      uri: <mongodb uri connection string>
```

### Demonstration data ###

By default, the application will generate demonstration data on startup if the DB is empty. To
prevent this behaviour you can edit `factorial-application-boot/src/main/resources/application.yml`
and remove the line for the `demo` profile from active profiles.

```yml
spring:
  profiles:
    active:
      - demo # remove this line
```

### Notes ###

* Maven version `3.6.3` was used during the development of the application.
* The version of Node.js used during development was `v14.17.1` with npm `6.14.13`.
