package org.lostrego.factorial.metric.usecase;

import org.lostrego.factorial.exception.MetricNotFoundException;
import org.lostrego.factorial.metric.Metric;

/**
 * Allows the removal of metrics
 */
public interface DeleteMetricUseCase {

    /**
     * Removes a {@link Metric} by the specified name
     *
     * @param name the name of the {@link Metric} to be removed
     * @throws MetricNotFoundException if no {@link Metric} by that name could be found
     */
    void deleteMetric(String name);
}
