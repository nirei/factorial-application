package org.lostrego.factorial.metric.usecase;

import java.util.List;
import org.lostrego.factorial.metric.Metric;

/**
 * Allows listing existing {@link Metric Metrics}
 */
public interface ListMetricsUseCase {

    /**
     * @return the list of existing {@link Metric Metrics}
     */
    List<Metric> listMetrics();

}
