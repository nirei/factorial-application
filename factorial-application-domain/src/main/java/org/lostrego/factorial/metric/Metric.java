package org.lostrego.factorial.metric;

import java.time.Instant;
import org.lostrego.factorial.exception.ApplicationErrorCode;
import org.lostrego.factorial.exception.ValidationException;
import org.lostrego.factorial.valueobject.TimeRange;

/**
 * A Metric is simply a tag that can take values in the form of {@link
 * org.lostrego.factorial.datapoint.Datapoint Datapoints} over time
 */
public class Metric {

    private final String name;

    private final int size;

    private final TimeRange timeRange;

    /**
     * Create a new Metric instance
     *
     * @param name  the name of the metric
     * @param size  the size of the metric
     * @param start the start of the metric
     * @param end   the end of the metric
     */
    public Metric(String name, int size, Instant start, Instant end) {
        this.name = name;
        this.size = size;
        this.timeRange = new TimeRange(start, end);
    }

    /**
     * Creates a new copy instance
     *
     * @param other the instance to be copied
     */
    public Metric(Metric other) {
        this(other.name, other.size, other.timeRange);
    }

    private Metric(String name, int size, TimeRange timeRange) {
        if (name == null) {
            throw new ValidationException(ApplicationErrorCode.NULL_METRIC_NAME,
                "Metric name cannot be null");
        }
        this.name = name;

        this.size = size;

        if (timeRange == null) {
            throw new ValidationException(ApplicationErrorCode.NULL_METRIC_TIME_RANGE,
                "Metric time range cannot be null");
        }
        this.timeRange = timeRange;
    }

    /**
     * @return the name of the metric
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return the size of the metric
     */
    public int getSize() {
        return this.size;
    }

    /**
     * @return the start of the metric
     */
    public Instant getStart() {
        return this.timeRange.getStart();
    }

    /**
     * @return the end of the metric
     */
    public Instant getEnd() {
        return this.timeRange.getFinish();
    }

    @Override
    public String toString() {
        return "Metric{" +
            "name='" + this.name + "'" +
            ", size=" + this.size +
            ", timeRange=" + this.timeRange +
            "}";
    }

    @Override
    @SuppressWarnings("MethodWithMultipleReturnPoints")
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        Metric metric = (Metric) obj;

        if (this.size != metric.size) {
            return false;
        }
        if (!this.name.equals(metric.name)) {
            return false;
        }
        return this.timeRange.equals(metric.timeRange);
    }

    @Override
    public int hashCode() {
        int result = this.name.hashCode();
        result = 31 * result + this.size;
        result = 31 * result + this.timeRange.hashCode();
        return result;
    }
}
