package org.lostrego.factorial.metric.usecase;

import org.lostrego.factorial.exception.MetricNotFoundException;
import org.lostrego.factorial.metric.Metric;

/**
 * Allows finding a {@link Metric} given its name
 */
public interface FindMetricUseCase {

    /**
     * @param name name of the {@link Metric to be found}
     * @return the found {@link Metric}
     * @throws MetricNotFoundException if the name does not match any known {@link Metric}
     */
    Metric findMetric(String name);
}
