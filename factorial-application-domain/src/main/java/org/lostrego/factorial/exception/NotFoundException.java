package org.lostrego.factorial.exception;

import java.util.NoSuchElementException;

/**
 * Raised when an entity cannot be found
 */
public class NotFoundException extends NoSuchElementException implements ApplicationErrorCode {

    private static final long serialVersionUID = -7808810826444211444L;
    private final String errorCode;

    protected NotFoundException(String errorCode, String s) {
        super(s);
        this.errorCode = errorCode;
    }

    @Override
    public String getErrorCode() {
        return this.errorCode;
    }
}
