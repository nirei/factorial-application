package org.lostrego.factorial.exception;

/**
 * Used to indicate a {@link org.lostrego.factorial.datapoint.Datapoint} could not be found
 */
public class DatapointNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 717630048491194328L;

    public DatapointNotFoundException(String errorCode, String s) {
        super(errorCode, s);
    }
}
