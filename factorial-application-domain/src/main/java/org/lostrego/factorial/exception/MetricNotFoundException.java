package org.lostrego.factorial.exception;

/**
 * Used to indicate a {@link org.lostrego.factorial.metric.Metric} could not be found
 */
public class MetricNotFoundException extends NotFoundException {

    private static final long serialVersionUID = -5390954931976782303L;

    public MetricNotFoundException(String errorCode, String s) {
        super(errorCode, s);
    }

}
