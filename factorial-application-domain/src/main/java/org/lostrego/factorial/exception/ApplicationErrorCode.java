package org.lostrego.factorial.exception;

@SuppressWarnings("StaticMethodOnlyUsedInOneClass")
public interface ApplicationErrorCode {

    String UNKNOWN_ERROR = "00";
    String NULL_DATAPOINT_METRIC_NAME = "10";
    String NULL_DATAPOINT_TIME = "11";
    String DATAPOINT_ALREADY_EXISTS = "12";
    String DATAPOINT_NOT_FOUND = "13";
    String NULL_METRIC_NAME = "20";
    String NULL_METRIC_TIME_RANGE = "21";
    String METRIC_NOT_FOUND = "23";
    String INVALID_TIME_RANGE = "30";
    String NULL_START_TIME_IN_RANGE = "31";
    String NULL_FINISH_TIME_IN_RANGE = "32";

    String getErrorCode();
}
