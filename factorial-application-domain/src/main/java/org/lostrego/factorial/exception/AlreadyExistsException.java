package org.lostrego.factorial.exception;

/**
 * Indicates an entity with the same identifier already exists and thus, another cannot be created
 */
public class AlreadyExistsException extends IllegalStateException implements ApplicationErrorCode {

    private static final long serialVersionUID = 7535635871773260956L;
    private final String errorCode;

    public AlreadyExistsException(String errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public AlreadyExistsException(String errorCode, String s) {
        super(s);
        this.errorCode = errorCode;
    }

    @Override
    public String getErrorCode() {
        return this.errorCode;
    }
}
