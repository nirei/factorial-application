package org.lostrego.factorial.exception;

/**
 * Used when a domain entity property has an illegal value or a combination thereof
 */
public class ValidationException extends IllegalArgumentException implements ApplicationErrorCode {

    private static final long serialVersionUID = 7675228439304291154L;
    private final String errorCode;

    public ValidationException(String errorCode, String s) {
        super(s);
        this.errorCode = errorCode;
    }

    @Override
    public String getErrorCode() {
        return this.errorCode;
    }
}
