package org.lostrego.factorial.datapoint;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import org.lostrego.factorial.exception.ApplicationErrorCode;
import org.lostrego.factorial.exception.ValidationException;

/**
 * Value of a {@link org.lostrego.factorial.metric.Metric} at a point in time
 */
public class Datapoint {

    public static final Comparator<Datapoint> COMPARATOR = Comparator
        .comparing(Datapoint::getName)
        .thenComparing(Datapoint::getTime);

    private final String name;

    private final Instant time;

    private final double value;

    /**
     * @param name  name of the metric for this datapoint
     * @param time  time at which this datapoint happened
     * @param value value of this datapoint
     */
    public Datapoint(String name, Instant time, double value) {
        if (name == null) {
            throw new ValidationException(ApplicationErrorCode.NULL_DATAPOINT_METRIC_NAME,
                "Metric name for a datapoint cannot be null");
        }
        this.name = name;

        if (time == null) {
            throw new ValidationException(ApplicationErrorCode.NULL_DATAPOINT_TIME,
                "Time for a datapoint cannot be null");
        }
        this.time = time.truncatedTo(ChronoUnit.MILLIS);

        this.value = value;
    }

    /**
     * Creates a new copy instance
     *
     * @param other the instance to be copied
     */
    public Datapoint(Datapoint other) {
        this(other.name, other.time, other.value);
    }

    /**
     * @return name of the metric for this datapoint
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return time of this datapoint
     */
    public Instant getTime() {
        return this.time;
    }

    /**
     * @return value of this datapoint
     */
    public double getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Datapoint{" +
            "name='" + this.name + "'" +
            ", time=" + this.time +
            ", value=" + this.value +
            "}";
    }

    @Override
    @SuppressWarnings("MethodWithMultipleReturnPoints")
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        Datapoint datapoint = (Datapoint) obj;

        if (Double.compare(datapoint.value, this.value) != 0) {
            return false;
        }
        if (!this.name.equals(datapoint.name)) {
            return false;
        }
        return this.time.equals(datapoint.time);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = this.name.hashCode();
        result = 31 * result + this.time.hashCode();
        temp = Double.doubleToLongBits(this.value);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
