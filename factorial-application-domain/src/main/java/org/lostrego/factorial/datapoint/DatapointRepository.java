package org.lostrego.factorial.datapoint;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.lostrego.factorial.metric.Metric;
import org.lostrego.factorial.valueobject.TimeRange;

/**
 * Describes data access logic for {@link Datapoint}
 */
public interface DatapointRepository {

    Optional<Datapoint> find(String name, Instant time);

    Datapoint save(Datapoint datapoint);

    Optional<Metric> findMetric(String name);

    List<Metric> findAllMetrics();

    void deleteMetric(String name);

    List<Datapoint> findDatapointsInTimeRange(String name, TimeRange timeRange);
}
