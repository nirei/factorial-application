package org.lostrego.factorial.datapoint.usecase;

import java.util.List;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.metric.Metric;
import org.lostrego.factorial.valueobject.TimeRange;

/**
 * Allows listing {@link Datapoint datapoints} for a given {@link Metric} in an specified {@link
 * TimeRange}
 */
public interface ListDatapointsUseCase {

    /**
     * @param name name of the {@link Metric}
     * @return list of {@link Datapoint datapoints} for that {@link Metric} in that {@link
     * TimeRange}
     */
    List<Datapoint> listDatapoints(String name, TimeRange timeRange);
}
