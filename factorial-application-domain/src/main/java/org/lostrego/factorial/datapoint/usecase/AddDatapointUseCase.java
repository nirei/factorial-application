package org.lostrego.factorial.datapoint.usecase;

import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.exception.AlreadyExistsException;

/**
 * Allows for the addition of a new datapoint to a metric
 */
public interface AddDatapointUseCase {

    /**
     * Add a new datapoint to a metric
     *
     * @param datapoint the datapoint to be created
     * @return the persisted datapoint
     * @throws AlreadyExistsException if a {@link Datapoint} for that metric and time already
     *                                exists
     */
    Datapoint addDatapoint(Datapoint datapoint);

}
