package org.lostrego.factorial.datapoint.usecase;

import java.time.Instant;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.exception.DatapointNotFoundException;
import org.lostrego.factorial.metric.Metric;

/**
 * Retrieves the value of a single datapoint by its metric's name and its time
 */
public interface FindDatapointUseCase {

    /**
     * @param name the name of the metric
     * @param time the datapoint's time
     * @return the datapoint
     * @throws DatapointNotFoundException it the time does not match any known {@link Datapoint} for
     *                                    that {@link Metric}
     */
    Datapoint findDatapoint(String name, Instant time);
}
