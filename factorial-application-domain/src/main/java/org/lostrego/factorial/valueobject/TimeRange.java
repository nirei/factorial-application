package org.lostrego.factorial.valueobject;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import org.lostrego.factorial.exception.ApplicationErrorCode;
import org.lostrego.factorial.exception.ValidationException;

/**
 * Represents a range between two times. Useful for queries on a timeline. Validates range being
 * positive.
 */
public class TimeRange {

    private final Instant start;

    private final Instant finish;

    /**
     * Build a new TimeRange given its parameters
     *
     * @param start  beginning of the time interval
     * @param finish end of the time interval
     */
    public TimeRange(Instant start, Instant finish) {
        if (start == null) {
            throw new ValidationException(ApplicationErrorCode.NULL_START_TIME_IN_RANGE,
                "Time range start date cannot be null");
        }

        if (finish == null) {
            throw new ValidationException(ApplicationErrorCode.NULL_FINISH_TIME_IN_RANGE,
                "Time range start date cannot be null");
        }

        if (start.isAfter(finish)) {
            var message = String.format(
                "Start date cannot be after finish date (start: %s, finish: %s)",
                start, finish);
            throw new ValidationException(ApplicationErrorCode.INVALID_TIME_RANGE, message);
        }
        this.start = start.truncatedTo(ChronoUnit.MILLIS);
        this.finish = finish.truncatedTo(ChronoUnit.MILLIS);
    }

    /**
     * Creates a new copy instance
     *
     * @param other the instance to be copied
     */
    public TimeRange(TimeRange other) {
        this(other.start, other.finish);
    }

    /**
     * @return beginning of the time range as a {@link OffsetDateTime}
     */
    public Instant getStart() {
        return this.start;
    }

    /**
     * @return end of the time range as a {@link OffsetDateTime}
     */
    public Instant getFinish() {
        return this.finish;
    }

    @Override
    public String toString() {
        return "TimeRange{" +
            "start=" + this.start +
            ", finish=" + this.finish +
            "}";
    }

    @Override
    @SuppressWarnings("MethodWithMultipleReturnPoints")
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        TimeRange timeRange = (TimeRange) obj;

        if (!this.start.equals(timeRange.start)) {
            return false;
        }
        return this.finish.equals(timeRange.finish);
    }

    @Override
    public int hashCode() {
        int result = this.start.hashCode();
        result = 31 * result + this.finish.hashCode();
        return result;
    }
}
