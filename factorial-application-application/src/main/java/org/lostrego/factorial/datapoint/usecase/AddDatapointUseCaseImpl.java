package org.lostrego.factorial.datapoint.usecase;

import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddDatapointUseCaseImpl implements AddDatapointUseCase {

    private static final Logger logger = LoggerFactory.getLogger(AddDatapointUseCaseImpl.class);

    private final DatapointRepository datapointRepository;

    @Autowired
    AddDatapointUseCaseImpl(DatapointRepository datapointRepository) {
        this.datapointRepository = datapointRepository;
    }

    @Override
    public Datapoint addDatapoint(Datapoint datapoint) {
        logger.info("Adding datapoint {}", datapoint);
        return this.datapointRepository.save(datapoint);
    }
}
