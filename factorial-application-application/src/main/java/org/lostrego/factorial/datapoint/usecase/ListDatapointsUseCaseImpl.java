package org.lostrego.factorial.datapoint.usecase;

import java.util.List;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.exception.ApplicationErrorCode;
import org.lostrego.factorial.exception.MetricNotFoundException;
import org.lostrego.factorial.valueobject.TimeRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ListDatapointsUseCaseImpl implements ListDatapointsUseCase {

    private static final Logger logger = LoggerFactory.getLogger(ListDatapointsUseCaseImpl.class);
    private final DatapointRepository datapointRepository;

    @Autowired
    ListDatapointsUseCaseImpl(DatapointRepository datapointRepository) {
        this.datapointRepository = datapointRepository;
    }

    @Override
    public List<Datapoint> listDatapoints(String name, TimeRange timeRange) {
        logger.debug("Listing datapoints by name {} and in time range {}", name, timeRange);

        if (this.datapointRepository.findMetric(name).isEmpty()) {
            throw new MetricNotFoundException(ApplicationErrorCode.METRIC_NOT_FOUND,
                "Unable to find metric " + name);
        }

        return this.datapointRepository.findDatapointsInTimeRange(name, timeRange);
    }
}
