package org.lostrego.factorial.datapoint.usecase;

import java.time.Instant;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.exception.ApplicationErrorCode;
import org.lostrego.factorial.exception.DatapointNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindDatapointUseCaseImpl implements FindDatapointUseCase {

    private static final Logger logger = LoggerFactory.getLogger(FindDatapointUseCaseImpl.class);
    private final DatapointRepository datapointRepository;

    @Autowired
    FindDatapointUseCaseImpl(DatapointRepository datapointRepository) {
        this.datapointRepository = datapointRepository;
    }

    @Override
    public Datapoint findDatapoint(String name, Instant time) {
        logger.debug("Finding datapoints by name {} and time {}", name, time);
        return this.datapointRepository.find(name, time).orElseThrow(
            () -> new DatapointNotFoundException(ApplicationErrorCode.DATAPOINT_NOT_FOUND,
                String.format("Unable to find datapoint for metric %s at %s", name, time)));
    }
}
