package org.lostrego.factorial.metric.usecase;

import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.exception.ApplicationErrorCode;
import org.lostrego.factorial.exception.MetricNotFoundException;
import org.lostrego.factorial.metric.Metric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindMetricUseCaseImpl implements FindMetricUseCase {

    private static final Logger logger = LoggerFactory.getLogger(FindMetricUseCaseImpl.class);
    private final DatapointRepository datapointRepository;

    @Autowired
    FindMetricUseCaseImpl(DatapointRepository datapointRepository) {
        this.datapointRepository = datapointRepository;
    }

    @Override
    public Metric findMetric(String name) {
        logger.debug("Finding metric by name {}", name);
        return this.datapointRepository.findMetric(name)
            .orElseThrow(() -> new MetricNotFoundException(ApplicationErrorCode.METRIC_NOT_FOUND,
                "No metric found for name " + name));
    }
}
