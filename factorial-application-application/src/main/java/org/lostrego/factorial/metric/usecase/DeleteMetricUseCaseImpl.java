package org.lostrego.factorial.metric.usecase;

import org.lostrego.factorial.datapoint.DatapointRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteMetricUseCaseImpl implements DeleteMetricUseCase {

    private static final Logger logger = LoggerFactory.getLogger(DeleteMetricUseCaseImpl.class);
    private final DatapointRepository datapointRepository;

    @Autowired
    DeleteMetricUseCaseImpl(DatapointRepository datapointRepository) {
        this.datapointRepository = datapointRepository;
    }

    @Override
    public void deleteMetric(String name) {
        logger.info("Deleting metric by name {}", name);
        this.datapointRepository.deleteMetric(name);
    }
}
