package org.lostrego.factorial.metric.usecase;

import java.util.List;
import org.lostrego.factorial.datapoint.DatapointRepository;
import org.lostrego.factorial.metric.Metric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ListMetricsUseCaseImpl implements ListMetricsUseCase {

    private static final Logger logger = LoggerFactory.getLogger(ListMetricsUseCaseImpl.class);
    private final DatapointRepository datapointRepository;

    @Autowired
    ListMetricsUseCaseImpl(DatapointRepository datapointRepository) {
        this.datapointRepository = datapointRepository;
    }

    @Override
    public List<Metric> listMetrics() {
        logger.debug("Listing metrics");
        return this.datapointRepository.findAllMetrics();
    }
}
