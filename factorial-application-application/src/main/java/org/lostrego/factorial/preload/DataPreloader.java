package org.lostrego.factorial.preload;

import java.time.Month;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import org.lostrego.factorial.datapoint.Datapoint;
import org.lostrego.factorial.datapoint.usecase.AddDatapointUseCase;
import org.lostrego.factorial.metric.usecase.ListMetricsUseCase;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Profile("demo")
public class DataPreloader {

    private static final String[] NAMES = {
        "Latency (ms)",
        "CPU (%)",
        "Memory (GB)",
        "Request size (KB)",
        "HDD Usage (%)",
        "Network usage (Kbps)"
    };

    private final AddDatapointUseCase addDatapointUseCase;
    private final ListMetricsUseCase listMetricsUseCase;
    private final Random random = new Random();

    public DataPreloader(AddDatapointUseCase addDatapointUseCase,
        ListMetricsUseCase listMetricsUseCase) {
        this.addDatapointUseCase = addDatapointUseCase;
        this.listMetricsUseCase = listMetricsUseCase;
    }

    @EventListener
    @SuppressWarnings("MagicNumber")
    public void onApplicationReady(ApplicationReadyEvent event) {
        if (!this.listMetricsUseCase.listMetrics().isEmpty()) {
            return;
        }

        for (String name : NAMES) {
            var amount = this.random.nextInt(1000) + 1000;

            var time = ZonedDateTime.of(
                2021, Month.OCTOBER.getValue(), 1,
                0, 0, 0, 0,
                ZoneOffset.ofHours(2));

            var value = this.random.nextDouble() * 99 + 1;

            for (int i = 0; i < amount; i++) {
                // This will do a random walk to make the data smoother than pure random
                value += this.random.nextDouble() * 4 - 2;

                this.addDatapointUseCase.addDatapoint(
                    new Datapoint(
                        name,
                        time.plus(i, ChronoUnit.MINUTES).toInstant(),
                        Math.abs(value)));
            }
        }
    }
}