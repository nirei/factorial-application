package org.lostrego.factorial.metric;

import io.swagger.annotations.ApiParam;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;
import org.lostrego.factorial.api.MetricApi;
import org.lostrego.factorial.metric.usecase.DeleteMetricUseCase;
import org.lostrego.factorial.metric.usecase.FindMetricUseCase;
import org.lostrego.factorial.metric.usecase.ListMetricsUseCase;
import org.lostrego.factorial.model.MetricDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MetricController implements MetricApi {

    private final DeleteMetricUseCase deleteMetricUseCase;
    private final FindMetricUseCase findMetricUseCase;
    private final ListMetricsUseCase listMetricsUseCase;

    @Autowired
    public MetricController(
        DeleteMetricUseCase deleteMetricUseCase,
        FindMetricUseCase findMetricUseCase,
        ListMetricsUseCase listMetricsUseCase) {
        this.deleteMetricUseCase = deleteMetricUseCase;
        this.findMetricUseCase = findMetricUseCase;
        this.listMetricsUseCase = listMetricsUseCase;
    }

    private static MetricDto mapToDto(Metric metric) {
        return new MetricDto().name(metric.getName())
            .size(metric.getSize())
            .start(OffsetDateTime.ofInstant(metric.getStart(), ZoneId.systemDefault()))
            .end(OffsetDateTime.ofInstant(metric.getEnd(), ZoneId.systemDefault()));
    }

    @Override
    public ResponseEntity<Void> deleteMetricName(
        @ApiParam(required = true) @PathVariable("name") String name) {
        this.deleteMetricUseCase.deleteMetric(name);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<MetricDto> getMetricName(
        @ApiParam(required = true) @PathVariable("name") String name) {
        var metric = this.findMetricUseCase.findMetric(name);
        var metricDto = mapToDto(metric);
        return ResponseEntity.ok(metricDto);
    }

    @Override
    public ResponseEntity<List<MetricDto>> listMetrics() {
        var metricDtos = this.listMetricsUseCase.listMetrics().stream()
            .map(MetricController::mapToDto)
            .collect(Collectors.toList());
        return ResponseEntity.ok(metricDtos);
    }
}
