package org.lostrego.factorial.exception;

import org.lostrego.factorial.model.InlineResponse500Dto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@SuppressWarnings("MethodMayBeStatic")
class ControllerExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(AlreadyExistsException.class)
    ResponseEntity<InlineResponse500Dto> handleAlreadyExistsException(
        AlreadyExistsException exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(
            new InlineResponse500Dto()
                .code(exception.getErrorCode())
                .description(exception.getMessage())
        );
    }

    @ExceptionHandler(NotFoundException.class)
    ResponseEntity<InlineResponse500Dto> handleNotFoundException(NotFoundException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
            new InlineResponse500Dto()
                .code(exception.getErrorCode())
                .description(exception.getMessage())
        );
    }

    @ExceptionHandler(ValidationException.class)
    ResponseEntity<InlineResponse500Dto> handleValidationException(ValidationException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
            new InlineResponse500Dto()
                .code(exception.getErrorCode())
                .description(exception.getMessage())
        );
    }

    @ExceptionHandler(Exception.class)
    ResponseEntity<InlineResponse500Dto> handleUnexpectedException(Exception exception) {
        logger.error("Unexpected exception handling request", exception);

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
            new InlineResponse500Dto()
                .code(ApplicationErrorCode.UNKNOWN_ERROR)
                .description(exception.getMessage())
        );
    }
}
