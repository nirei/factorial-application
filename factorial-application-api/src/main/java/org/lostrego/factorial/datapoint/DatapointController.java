package org.lostrego.factorial.datapoint;

import io.swagger.annotations.ApiParam;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.lostrego.factorial.api.DatapointApi;
import org.lostrego.factorial.datapoint.usecase.AddDatapointUseCase;
import org.lostrego.factorial.datapoint.usecase.FindDatapointUseCase;
import org.lostrego.factorial.datapoint.usecase.ListDatapointsUseCase;
import org.lostrego.factorial.model.DatapointDto;
import org.lostrego.factorial.valueobject.TimeRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DatapointController implements DatapointApi {

    private final AddDatapointUseCase addDatapointUseCase;
    private final FindDatapointUseCase findDatapointUseCase;
    private final ListDatapointsUseCase listDatapointsUseCase;

    @Autowired
    public DatapointController(
        AddDatapointUseCase addDatapointUseCase,
        FindDatapointUseCase findDatapointUseCase,
        ListDatapointsUseCase listDatapointsUseCase) {
        this.addDatapointUseCase = addDatapointUseCase;
        this.findDatapointUseCase = findDatapointUseCase;
        this.listDatapointsUseCase = listDatapointsUseCase;
    }

    private static DatapointDto mapToDto(Datapoint datapoint) {
        return new DatapointDto()
            .metric(datapoint.getName())
            .time(OffsetDateTime.ofInstant(datapoint.getTime(), ZoneId.systemDefault()))
            .value(datapoint.getValue());
    }

    private static Datapoint mapToDatapoint(DatapointDto datapointDto) {
        return new Datapoint(
            datapointDto.getMetric(),
            Objects.requireNonNullElseGet(datapointDto.getTime(), OffsetDateTime::now).toInstant(),
            datapointDto.getValue());
    }

    @Override
    public ResponseEntity<DatapointDto> getMetricNamePointTime(
        @ApiParam(required = true) @PathVariable("name") String name,
        @ApiParam(required = true) @PathVariable("time") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime time) {

        return ResponseEntity.ok(
            mapToDto(this.findDatapointUseCase.findDatapoint(name, time.toInstant())));
    }

    @Override
    public ResponseEntity<List<DatapointDto>> listMetricNamePoints(
        @ApiParam(required = true) @PathVariable("name") String name,
        @NotNull @ApiParam(value = "Earliest date for a datapoint to be included in the response", required = true) @Valid @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime from,
        @NotNull @ApiParam(value = "Latest date for a datapoint to be included in the response", required = true) @Valid @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime to) {

        var timeRange = new TimeRange(from.toInstant(), to.toInstant());
        var datapoints = this.listDatapointsUseCase.listDatapoints(name, timeRange).stream()
            .map(DatapointController::mapToDto);
        return ResponseEntity.ok(datapoints.collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<DatapointDto> postMetricNamePoint(
        @ApiParam(required = true) @PathVariable("name") String name,
        @ApiParam @Valid @RequestBody(required = false) DatapointDto datapointDto) {

        // Replace metric name with path variable value
        datapointDto.setMetric(name);

        var datapoint = mapToDatapoint(datapointDto);
        var created = this.addDatapointUseCase.addDatapoint(datapoint);
        var responseDto = mapToDto(created);
        return ResponseEntity.ok(responseDto);
    }
}
